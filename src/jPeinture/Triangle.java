package jPeinture;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Stroke;


public class Triangle extends Formes{
	
	Polygon polygon;
	
	public Triangle(Color c, BasicStroke stroke, Point origine){
		super(c, stroke, origine);
	}
	
	public void draw(Point p2){
		// Je dessine un triangle isosèle
		polygon = new Polygon();
		polygon.addPoint(ori.x, ori.y);
		polygon.addPoint(p2.x, p2.y);
		// x3 est a égale distance avec le centre
		polygon.addPoint(ori.x - (p2.x - ori.x), p2.y);
	}

	@Override
	public void print(Graphics gr) {
		gr.drawPolygon(polygon);		
	}
}
