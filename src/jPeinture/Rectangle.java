package jPeinture;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Stroke;
import java.util.Vector;

public class Rectangle extends Formes{

	Polygon polygon;
	
	public Rectangle(Color c, BasicStroke stroke, Point origine){
		super(c, stroke, origine);
	}
	
	public Polygon getPolygon(){
		return polygon;
	}
	
	public void draw(Point p2){
		polygon = new Polygon();
		polygon.addPoint(ori.x, ori.y);
		polygon.addPoint(p2.x, ori.y);
		polygon.addPoint(p2.x, p2.y);
		polygon.addPoint(ori.x, p2.y);
	}
	
	@Override
	public void print(Graphics gr) {
		gr.drawPolygon(polygon);		
	}
	
	
}
