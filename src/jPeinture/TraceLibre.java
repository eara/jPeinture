package jPeinture;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Stroke;
import java.util.Vector;

public class TraceLibre extends Formes{
		
	private Vector<Point> trace;
	
	public TraceLibre(Color c, BasicStroke stroke, Point origine){
		super(c, stroke, origine);
		trace = new Vector<Point>();
		trace.add(origine);
	}
	
	public void draw(Point p){
		trace.add(p);
	}
	
	@Override
	public void print(Graphics gr) {
		for(int i = 1 ; i < trace.size() ; i++)
			gr.drawLine(trace.get(i - 1).x, trace.get(i - 1).y , trace.get(i).x, trace.get(i).y);
	}
}
