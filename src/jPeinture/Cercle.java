package jPeinture;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Stroke;

public class Cercle extends Formes{
	
	private int width;
	private int height;
	private Point tmp;
	
	public Cercle(Color c, BasicStroke stroke, Point origine){
		super(c, stroke, origine);
		tmp = origine;
	}
	
	public void draw(Point p2){
		width = p2.x - ori.x;
		height = p2.y - ori.y;
		// Pour dessiner un cercle � l'envers
		if (width < 0 || height < 0){
			ori = p2;
			p2 = tmp;
			width = p2.x - ori.x;
			height = p2.y - ori.y;
		}
		else{
			ori = tmp;
		}
	}

	@Override
	public void print(Graphics gr) {
		gr.drawOval(ori.x, ori.y, width, height);
	}
}
