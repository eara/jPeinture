package jPeinture;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;


import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;


public class Peinture extends JFrame {
	
	// Panels and Containers
	private JPanel contentPane;
	private JPanel northPanel;
	private JPanel southPanel;
	private JPanel westPanel;
	private JPanel eastPanel;
	private SurfaceDessin canvas;
	private JPanel panelOutils;
	private JPanel panelForme;
	private JPanel panelCouleur;
	private JPanel grilleCouleur;
	
	// Buttons and Groups
	// Groupe outils
	private ButtonGroup groupeOutils;
	private JToggleButton effaceBTN;
	private JToggleButton crayonBTN;
	private JToggleButton pipetteBTN;
	private JToggleButton bucketBTN;
	private JButton saveBTN;
	
	private JToggleButton cercleBTN;
	private JToggleButton carreBTN;
	private JToggleButton triangleBTN;
	
	// Goupe couleur
	private ButtonGroup groupeCouleur;
	private JToggleButton leftColorBTN;
	private JToggleButton rightColorBTN;
	private JLabel color1Lbl;
	private JLabel color2Lbl;
	
	// Palette de couleur
	private JLabel noirLbl;
	private JLabel blancLbl;
	private JLabel rougeLbl;
	private JLabel orangeLbl;
	private JLabel jauneLbl;
	private JLabel vertLbl;
	private JLabel bleuLbl;
	private JLabel cyanLbl;
	private JLabel roseLbl;
	private JLabel grisLbl;
	
	// Stroke
	private JTextField strokeField;
	private JLabel lblTailleDuTrait;
	
	// Listeners
	private Ecouteur ec;
	private ColorEvent evtColor;
	
	// Curseur (Manque de temps)
	//Toolkit tk;
	//Cursor curseur;
	//Image imageCursor;
	
	// Varaiable de controles
	private Formes tmpForme;
	private Vector<Formes> mesFormes;
	private Color bgCouleur;
	private JFileChooser fileChooser;
	private BufferedImage bfImg;
	
	/**
	 * Create the frame.
	 */
	public Peinture() {
		// Auto-generated
		setTitle("   j'Peinture");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 824, 534);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		// MY CODE
		bgCouleur = Color.WHITE;
		
		// Cr�ation de l'�couteur
		ec = new Ecouteur();
		evtColor = new ColorEvent();
		
		// Fill BorderLayout with 4 surrounding JPanel and my custom drawing
		northPanel = new JPanel();
		northPanel.setPreferredSize(new Dimension(800, 80));
		eastPanel = new JPanel();
		southPanel = new JPanel();
		westPanel = new JPanel();
		canvas = new SurfaceDessin();
		// outils
		panelOutils = new JPanel();
		panelOutils.setBounds(12, 5, 236, 46);
		panelForme = new JPanel();
		panelForme.setBounds(260, 5, 147, 60);
		panelCouleur = new JPanel();
		panelCouleur.setBounds(419, 5, 230, 60);
		grilleCouleur = new JPanel();
		grilleCouleur.setBounds(661, 11, 108, 40);
		
		// Add panel to container
		contentPane.add(northPanel, BorderLayout.NORTH);
		contentPane.add(eastPanel, BorderLayout.EAST);
		contentPane.add(southPanel, BorderLayout.SOUTH);
		contentPane.add(westPanel, BorderLayout.WEST);
		getContentPane().add(canvas, BorderLayout.CENTER);
		northPanel.setLayout(null);
		northPanel.add(panelOutils);
		northPanel.add(panelForme);
		northPanel.add(panelCouleur);
		northPanel.add(grilleCouleur);
		grilleCouleur.setLayout(new GridLayout(0, 5, 2, 2));
		
		// Incrire la source � l'�couteur
		canvas.addMouseListener( ec );
		
		// Creation of Toggle buttons for tools
		groupeOutils = new ButtonGroup();
		groupeCouleur = new ButtonGroup();
		
		crayonBTN = new JToggleButton( new ImageIcon("icones/crayon.gif"));
		crayonBTN.setPreferredSize(new Dimension(34, 34));
		panelOutils.add(crayonBTN);
		groupeOutils.add(crayonBTN);
		
		effaceBTN = new JToggleButton( new ImageIcon("icones/efface.gif"));
		effaceBTN.setPreferredSize(new Dimension(34, 34));
		panelOutils.add(effaceBTN);
		groupeOutils.add(effaceBTN);
		
		pipetteBTN = new JToggleButton( new ImageIcon("icones/pipette.gif"));
		pipetteBTN.setPreferredSize(new Dimension(34, 34));
		panelOutils.add(pipetteBTN);
		groupeOutils.add(pipetteBTN);
		
		bucketBTN = new JToggleButton( new ImageIcon("icones/remplissage.gif"));
		bucketBTN.setPreferredSize(new Dimension(34, 34));
		panelOutils.add(bucketBTN);
		groupeOutils.add(bucketBTN);		
		
		cercleBTN = new JToggleButton( new ImageIcon("icones/cercle.gif"));
		cercleBTN.setPreferredSize(new Dimension(34, 34));
		panelForme.add(cercleBTN);
		groupeOutils.add(cercleBTN);
		
		carreBTN = new JToggleButton( new ImageIcon("icones/rectangle.gif"));
		carreBTN.setPreferredSize(new Dimension(34, 34));
		panelForme.add(carreBTN);
		groupeOutils.add(carreBTN);
		
		triangleBTN = new JToggleButton( new ImageIcon("icones/triangle.gif"));
		triangleBTN.setPreferredSize(new Dimension(34, 34));
		panelForme.add(triangleBTN);
		groupeOutils.add(triangleBTN);
		
		// Selectionner le crayon par d�faut
		crayonBTN.setSelected(true);
		
		saveBTN = new JButton( new ImageIcon("icones/save.gif"));
		saveBTN.setPreferredSize(new Dimension(34, 34));
		panelOutils.add(saveBTN);
		saveBTN.addMouseListener( ec );
		
		// Bouttons pour couleur
		rightColorBTN = new JToggleButton("couleur 1");
		rightColorBTN.setPreferredSize(new Dimension(100, 50));
		FlowLayout fl_rightColorBTN = new FlowLayout();
		rightColorBTN.setLayout(fl_rightColorBTN);
		rightColorBTN.setVerticalAlignment(SwingConstants.BOTTOM);
		panelCouleur.add(rightColorBTN);
		groupeCouleur.add(rightColorBTN);
		color1Lbl = new JLabel("");
		color1Lbl.setVerticalAlignment(SwingConstants.BOTTOM);
		color1Lbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		color1Lbl.setPreferredSize(new Dimension(60, 15));
		color1Lbl.setBackground(Color.BLACK);
		color1Lbl.setOpaque(true);
		rightColorBTN.add(color1Lbl);
		rightColorBTN.setSelected(true);
		
		leftColorBTN = new JToggleButton("couleur 2");
		leftColorBTN.setPreferredSize(new Dimension(100, 50));
		leftColorBTN.setLayout(new FlowLayout());
		leftColorBTN.setVerticalAlignment(SwingConstants.BOTTOM);
		panelCouleur.add(leftColorBTN);
		groupeCouleur.add(leftColorBTN);
		color2Lbl = new JLabel("");
		color2Lbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		color2Lbl.setPreferredSize(new Dimension(60, 15));
		color2Lbl.setBackground(Color.GREEN);
		color2Lbl.setOpaque(true);
		leftColorBTN.add(color2Lbl);
		
		// Label de couleurs
		noirLbl = new JLabel(" ");
		noirLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		noirLbl.setOpaque(true);
		noirLbl.setPreferredSize(new Dimension(20, 20));
		noirLbl.setBackground(Color.BLACK);
		grilleCouleur.add(noirLbl);
		noirLbl.addMouseListener( evtColor );
		
		blancLbl = new JLabel(" ");
		blancLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		blancLbl.setOpaque(true);
		grilleCouleur.add(blancLbl);
		blancLbl.setPreferredSize(new Dimension(20, 20));
		blancLbl.setBackground(Color.WHITE);
		blancLbl.addMouseListener( evtColor );
		
		rougeLbl = new JLabel(" ");
		rougeLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		rougeLbl.setOpaque(true);
		rougeLbl.setBackground(Color.RED);
		rougeLbl.setPreferredSize(new Dimension(20, 20));
		grilleCouleur.add(rougeLbl);
		rougeLbl.addMouseListener( evtColor );
		
		orangeLbl = new JLabel("");
		orangeLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		orangeLbl.setOpaque(true);
		orangeLbl.setBackground(Color.ORANGE);
		grilleCouleur.add(orangeLbl);
		orangeLbl.addMouseListener( evtColor );
		
		jauneLbl = new JLabel("");
		jauneLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		jauneLbl.setPreferredSize(new Dimension(20, 20));
		jauneLbl.setOpaque(true);
		jauneLbl.setBackground(Color.YELLOW);
		grilleCouleur.add(jauneLbl);
		jauneLbl.addMouseListener( evtColor );
		
		vertLbl = new JLabel("");
		vertLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		vertLbl.setPreferredSize(new Dimension(20, 20));
		vertLbl.setOpaque(true);
		vertLbl.setBackground(Color.GREEN);
		grilleCouleur.add(vertLbl);
		vertLbl.addMouseListener( evtColor );
		
		bleuLbl = new JLabel("");
		bleuLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		bleuLbl.setPreferredSize(new Dimension(20, 20));
		bleuLbl.setOpaque(true);
		bleuLbl.setBackground(Color.BLUE);
		grilleCouleur.add(bleuLbl);
		bleuLbl.addMouseListener( evtColor );
		
		cyanLbl = new JLabel("");
		cyanLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		cyanLbl.setPreferredSize(new Dimension(20, 20));
		cyanLbl.setOpaque(true);
		cyanLbl.setBackground(Color.CYAN);
		grilleCouleur.add(cyanLbl);
		cyanLbl.addMouseListener( evtColor );
		
		roseLbl = new JLabel("");
		roseLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		roseLbl.setBackground(Color.PINK);
		roseLbl.setPreferredSize(new Dimension(20, 20));
		roseLbl.setOpaque(true);
		grilleCouleur.add(roseLbl);
		roseLbl.addMouseListener( evtColor );
		
		grisLbl = new JLabel("");
		grisLbl.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		grisLbl.setPreferredSize(new Dimension(20, 20));
		grisLbl.setOpaque(true);
		grisLbl.setBackground(Color.GRAY);
		grilleCouleur.add(grisLbl);
		grisLbl.addMouseListener( evtColor );
		
		// Stroke UI
		lblTailleDuTrait = new JLabel("Taille du Trait:");
		lblTailleDuTrait.setBounds(59, 57, 90, 16);
		northPanel.add(lblTailleDuTrait);
		
		strokeField = new JTextField();
		strokeField.setBounds(152, 55, 57, 20);
		northPanel.add(strokeField);
		strokeField.setColumns(4);
		strokeField.setText("15");
		
		// Inscription des sources à l'écouteur
		canvas.addMouseListener( ec );
		canvas.addMouseMotionListener( ec );		
		
		// JFileChoser
		fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("   Sauvegarder");
		
		// J'ai manqu� de temps pour le curseur
		// Curseur
		//tk = Toolkit.getDefaultToolkit();
		
		// Formes
		mesFormes = new Vector<Formes>();
	}
	
	public BufferedImage getDessin(JPanel canvas){
		BufferedImage img = new BufferedImage(canvas.getWidth(), canvas.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = img.createGraphics();
		canvas.paint(g2);
		return img;
	}
	
	private class SurfaceDessin extends JPanel{
		
		@Override
		protected void paintComponent(Graphics gr){
			super.paintComponent(gr);
			setBackground(bgCouleur);
			
			Graphics2D g2 = (Graphics2D) gr;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			
			for( Formes forme : mesFormes){
				if(forme instanceof Efface)
					forme.setCouleur(bgCouleur);
				
				g2.setColor(forme.getCouleur());
				g2.setStroke(forme.getStroke());
				forme.print(g2);
			}
			
			if(tmpForme != null){
				gr.setColor(tmpForme.getCouleur());
				g2.setStroke(tmpForme.getStroke());
				tmpForme.print(g2);
			}
		}
	}
	
	private class Ecouteur implements MouseListener, MouseMotionListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			System.out.println("mouseCliked");
			// Simple clique
			// Outils du seau
			if(bucketBTN.isSelected()){
				if(e.getButton() == MouseEvent.BUTTON1)
					bgCouleur = color1Lbl.getBackground();
				else if(e.getButton() == MouseEvent.BUTTON3)
					bgCouleur = color2Lbl.getBackground();
			}
			else if(pipetteBTN.isSelected()){
				// Optenir la couleur � l'aide d'un BufferedImage
				Color c = new Color(getDessin(canvas).getRGB(e.getX(), e.getY()));
				if(rightColorBTN.isSelected())
					color1Lbl.setBackground(c);
				else
					color2Lbl.setBackground(c);
				crayonBTN.setSelected(true);
			}
			// Sauvegarder
			else if (e.getSource() == saveBTN){
				int selection = fileChooser.showSaveDialog(Peinture.this);
				if(selection == JFileChooser.APPROVE_OPTION){	
					try {
						File fichier = fileChooser.getSelectedFile();
						ImageIO.write( getDessin(canvas), "png", new File( fichier.getAbsolutePath() + ".png"));
					} catch(IOException exep){
						exep.printStackTrace();
					}
				}
			}// ENDIF
		}// END mouseClicked
		
		@Override
		public void mousePressed(MouseEvent e) {
			System.out.println("mousePressed");
			// Set caracteristics
			Color couleur;
			if(e.getButton() == MouseEvent.BUTTON1)
				 couleur =  color1Lbl.getBackground();	
			else if(e.getButton() == MouseEvent.BUTTON3)
				couleur =  color2Lbl.getBackground();
			else
				couleur = Color.BLACK;
			
			BasicStroke stroke = new BasicStroke(new Float(strokeField.getText()), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			Point point = e.getPoint();
			
			// Voir quels outils est sélecéionné
			if( crayonBTN.isSelected()){
				tmpForme = new TraceLibre(couleur, stroke , point);
			}
			else if ( effaceBTN.isSelected()){
				System.out.println("efface");
				tmpForme = new Efface(bgCouleur, stroke, point);
			}
			else if ( carreBTN.isSelected()){
				System.out.println("Rectangles");
				tmpForme = new jPeinture.Rectangle(couleur, stroke, point);
			}
			else if ( cercleBTN.isSelected() ){
				System.out.println("Cercle");
				tmpForme = new Cercle(couleur, stroke, point);
			}
			else if ( triangleBTN.isSelected() ){
				System.out.println("Triangle");
				tmpForme = new Triangle(couleur, stroke, point);
			}			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			System.out.println("mouseReleased");
			if(tmpForme != null)
				mesFormes.add(tmpForme);
			canvas.repaint();
		}
		
		@Override
		public void mouseDragged(MouseEvent e) {	
			if(tmpForme != null)
				tmpForme.draw(new Point(e.getX(), e.getY()));
			canvas.repaint();
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO
		}
		@Override
		public void mouseExited(MouseEvent e) {
			// TODO
		}
		@Override
		public void mouseMoved(MouseEvent e) {
			//TODO
		}
	}
	
	private class ColorEvent extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent e){
			if(rightColorBTN.isSelected())
				color1Lbl.setBackground(((JLabel)e.getSource()).getBackground());
			else
				color2Lbl.setBackground(((JLabel)e.getSource()).getBackground());
		}
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Peinture frame = new Peinture();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
