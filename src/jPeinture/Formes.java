package jPeinture;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Stroke;



public abstract class Formes {
	
	//protected Vector<Point> point;
	private Color couleur;
	private BasicStroke stroke;
	protected Point ori; // ori = origine
	
	public Formes(Color couleur, BasicStroke stroke, Point origine){
		//point =  new Vector<Point>();
		this.stroke = stroke;
		this.couleur = couleur;
		ori = origine;
	}
	
	public void setCouleur(Color c){
		couleur = c;
	}
	
	public Color getCouleur(){
		return couleur;
	}
	
	public void setStroke(BasicStroke stroke){
		this.stroke = stroke;
	}
	
	public Stroke getStroke(){
		return stroke;
	}
	// M�thode abstraite afin de faire l'h�ritage dans l'�couteur
	abstract public void draw(Point p);
	abstract public void print(Graphics gr);
}